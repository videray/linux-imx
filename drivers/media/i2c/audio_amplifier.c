/*
 * Copyright (C) 2018 e-con Systems Pvt Ltd, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/i2c.h>
#include <linux/seq_file.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/delay.h>

#define AUDIO_ADDR 0x7C

typedef struct audio_amp_reg{
	uint16_t reg;
	uint8_t val;
}AUDIO_AMP_PARSE;

AUDIO_AMP_PARSE AUDIO_AMP_CONF[] = {
	{0x04, 0x9F},
	{0x03, 0x7F},
	{0x02, 0x40},
	{0x01, 0x36},
	{0x00, 0x1C},
};

int audio_amp_write_i2c(struct i2c_client *client, u16 sladdr,  u8 * val, u32 count)
{
	int ret;

	struct i2c_msg msg = {
		.addr = sladdr,
		.flags = 0,
		.len = count,
		.buf = val,
	};

	ret = i2c_transfer(client->adapter, &msg, 1);
	if (ret < 0) {
		dev_err(&client->dev, "Failed writing register ret = %d!\n",
				ret);
		return ret;
	}
	return 0;
}

s32 audio_amp_write_8b_reg(struct i2c_client *client, u16 sladdr, u8 reg, u8 val)
{
	u8 bcount = 2;
	u8 au8Buf[2] = { 0 };

	au8Buf[0] = reg;
	au8Buf[1] = val;

	if (audio_amp_write_i2c(client, sladdr,au8Buf, bcount) < 0) {
		dev_err(&client->dev,
				"%s:write reg error: reg = 0x%x,val = 0x%x\n", __func__,
				reg, val);
		return -EIO;
	}

	return 0;
}

static s32 audio_amp_parse_regdata(struct i2c_client *client, AUDIO_AMP_PARSE * regdata,
		u32 reg_cnt)
{
	int i = 0;

	for (i = 0; i < reg_cnt; i++) {

		if ((audio_amp_write_8b_reg(client, AUDIO_ADDR, regdata[i].reg, regdata[i].val)) <
				0) {
			dev_err(&client->dev, "%s(%d): Failed \n",
					__func__, __LINE__);
			return -EIO;
		}
		msleep(1);

	}

	return 0;
}

static int audio_amp_probe(struct i2c_client *client,
		const struct i2c_device_id *id)
{

	dev_info(&client->dev, "Probing for Audio-Amplifier\n");

	/* Configuring Audio Amplifier */
	if(audio_amp_parse_regdata(client, AUDIO_AMP_CONF, ARRAY_SIZE(AUDIO_AMP_CONF)) < 0) {
		dev_err(&client->dev, "%s: Failed to configure Audio-AMplifier\n",__func__);
		return -EIO;
	}
	dev_info(&client->dev,"Configuring Audio-Amplifier Successful\n");

	return 0;
}

static int audio_amp_remove(struct i2c_client *client)
{
	return 0;
}
#ifdef CONFIG_OF

static const struct of_device_id audio_amp_of_match[] = {
	{
		.compatible = "econ,audio-amplifier",
		.data = (void*) 0,
	},
	{}
};

MODULE_DEVICE_TABLE(of, audio_amp_of_match);

#endif

static const struct i2c_device_id audio_amp_id[] = {
	{ "audio-amplifier", 0 },
	{ }
};

MODULE_DEVICE_TABLE(i2c, audio_amp_id);

static struct i2c_driver audio_amp_i2c_driver = {
	.driver = {
		.name = "audio-amplifier",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(audio_amp_of_match),
	},
	.probe = audio_amp_probe,
	.remove = audio_amp_remove,
	.id_table = audio_amp_id,
};

module_i2c_driver(audio_amp_i2c_driver);

MODULE_DESCRIPTION("Audio Amplifier Controller Driver");
MODULE_AUTHOR("e-con Systems");
MODULE_LICENSE("GPL v2");
