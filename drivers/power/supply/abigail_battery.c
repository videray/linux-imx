

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/idr.h>
#include <linux/power_supply.h>

#include "abigail_i2c.h"

#define POLL_DELAY (1*HZ)


#define POWER_SUPPLY_PROP_VOLTAGE_NOW_VALUE 3600
#define POWER_SUPPLY_PROP_CURRENT_NOW_VALUE 400000
#define POWER_SUPPLY_PROP_CYCLE_COUNT_VALUE 32
#define POWER_SUPPLY_PROP_TEMP_VALUE 350
#define POWER_SUPPLY_PROP_CHARGE_FULL_VALUE 4000000
#define POWER_SUPPLY_PROP_CHARGE_COUNTER_VALUE 1900000
#define POWER_SUPPLY_PROP_CONSTANT_CHARGE_CURRENT_MAX_VALUE 500000
#define POWER_SUPPLY_PROP_CONSTANT_CHARGE_VOLTAGE_MAX_VALUE 5000000


struct abigail_info {
    struct i2c_client *client;
    struct power_supply *battery;
    struct power_supply_desc battery_desc;
    struct power_supply *usb;
    struct power_supply_desc usb_desc;
    struct delayed_work bat_work_queue;
    int id;
    int capacity;
    int status;
};

static DEFINE_IDR(battery_id);
static DEFINE_MUTEX(battery_lock);

static int abigail_bat_update(struct abigail_info *info)
{
    int ret;
    u8 write_buf[] = {(GET_BATT_SOC >> 8), 0xFF & GET_BATT_SOC};
    u8 read_buf[2];
    struct i2c_msg msg;
    int ac_plugged;


    struct i2c_msg msgs[] = {
        {
            .addr = info->client->addr,
            .flags = 0,
            .len = 2,
            .buf = write_buf,
        },
        {
            .addr = info->client->addr,
            .flags = I2C_M_RD,
            .len = 2,
            .buf = read_buf,
        }
    };

    if ((ret = i2c_transfer(info->client->adapter, msgs, 2) == 2)) {
        ac_plugged = read_buf[0];
        info->capacity = read_buf[1];

        if (info->capacity == 100) {
            info->status = POWER_SUPPLY_STATUS_FULL;
        } else {
            info->status = ac_plugged ? POWER_SUPPLY_STATUS_CHARGING : POWER_SUPPLY_STATUS_DISCHARGING;
        }
        ret = 0;

    } else {
        dev_err(&info->client->dev, "error reading to i2c bus: %d\n",
                                                        ret);
    }

    return ret;

}

static void abigail_bat_work(struct work_struct *work)
{
    struct abigail_info *info;

    info = container_of(work, struct abigail_info, bat_work_queue.work);

    abigail_bat_update(info);

    schedule_delayed_work(&info->bat_work_queue, POLL_DELAY);
}

static enum power_supply_property abigail_usb_props[] = {
    POWER_SUPPLY_PROP_ONLINE,
};

static int abigail_usb_get_property(struct power_supply *psy,
                                  enum power_supply_property psp,
                                  union power_supply_propval *val)
{
    int ret = 0;
    switch (psp) {
        case POWER_SUPPLY_PROP_ONLINE:
            val->intval = 1;
            break;
        default:
            ret = -EINVAL;
            break;
    }
    return ret;
}

static int abigail_battery_get_property(struct power_supply *psy,
				       enum power_supply_property prop,
				       union power_supply_propval *val)
{
    struct abigail_info *info = power_supply_get_drvdata(psy);
    int ret = 0;

    switch (prop) {
    case POWER_SUPPLY_PROP_STATUS:
        val->intval = info->status;
        break;

    case POWER_SUPPLY_PROP_HEALTH:
        val->intval = POWER_SUPPLY_HEALTH_GOOD;
        break;

    case POWER_SUPPLY_PROP_PRESENT:
        val->intval = 1;
        break;

    case POWER_SUPPLY_PROP_TECHNOLOGY:
        val->intval = POWER_SUPPLY_TECHNOLOGY_LION;
        break;

    case POWER_SUPPLY_PROP_CAPACITY:
        val->intval = info->capacity;
        break;

    case POWER_SUPPLY_PROP_VOLTAGE_NOW:
        val->intval = POWER_SUPPLY_PROP_VOLTAGE_NOW_VALUE;
        break;
    
    case POWER_SUPPLY_PROP_CURRENT_NOW:
        val->intval = POWER_SUPPLY_PROP_CURRENT_NOW_VALUE;
        break;
    
    case POWER_SUPPLY_PROP_CYCLE_COUNT:
        val->intval = POWER_SUPPLY_PROP_CYCLE_COUNT_VALUE;
        break;

    case POWER_SUPPLY_PROP_TEMP:
        val->intval = POWER_SUPPLY_PROP_TEMP_VALUE;
        break;

    case POWER_SUPPLY_PROP_CHARGE_FULL:
        val->intval = POWER_SUPPLY_PROP_CHARGE_FULL_VALUE;
        break;

    case POWER_SUPPLY_PROP_CHARGE_COUNTER:
        val->intval = POWER_SUPPLY_PROP_CHARGE_COUNTER_VALUE;
        break;

    case POWER_SUPPLY_PROP_CONSTANT_CHARGE_CURRENT_MAX:
        val->intval = POWER_SUPPLY_PROP_CONSTANT_CHARGE_CURRENT_MAX_VALUE;
        break;

    case POWER_SUPPLY_PROP_CONSTANT_CHARGE_VOLTAGE_MAX:
        val->intval = POWER_SUPPLY_PROP_CONSTANT_CHARGE_VOLTAGE_MAX_VALUE;
        break;

    case POWER_SUPPLY_PROP_ONLINE:
        val->intval = 1;
        break;

    default:
        ret = -EINVAL;

    }

    return ret;
}

static enum power_supply_property abigail_battery_props[] = {
	POWER_SUPPLY_PROP_STATUS,
    POWER_SUPPLY_PROP_HEALTH,
    POWER_SUPPLY_PROP_PRESENT,
    POWER_SUPPLY_PROP_TECHNOLOGY,
    POWER_SUPPLY_PROP_CAPACITY,
    POWER_SUPPLY_PROP_VOLTAGE_NOW,
    POWER_SUPPLY_PROP_CURRENT_NOW,
    POWER_SUPPLY_PROP_CYCLE_COUNT,
    POWER_SUPPLY_PROP_TEMP,
    POWER_SUPPLY_PROP_CHARGE_FULL,
    POWER_SUPPLY_PROP_CHARGE_COUNTER,
    POWER_SUPPLY_PROP_CONSTANT_CHARGE_CURRENT_MAX,
    POWER_SUPPLY_PROP_CONSTANT_CHARGE_VOLTAGE_MAX,
    POWER_SUPPLY_PROP_ONLINE,
};

static void abigail_power_supply_init(struct power_supply_desc *battery)
{
	battery->type			= POWER_SUPPLY_TYPE_BATTERY;
	battery->properties		= abigail_battery_props;
	battery->num_properties		= ARRAY_SIZE(abigail_battery_props);
	battery->get_property		= abigail_battery_get_property;
	battery->external_power_changed	= NULL;
}

static int abigail_battery_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    int ret;
    int num;
    struct abigail_info *info;
    struct power_supply_config psy_cfg = {};

    /* Get an ID for this battery */
	mutex_lock(&battery_lock);
	ret = idr_alloc(&battery_id, client, 0, 0, GFP_KERNEL);
	mutex_unlock(&battery_lock);
	if (ret < 0)
		goto fail_id;
    num = ret;

    info = kzalloc(sizeof(*info), GFP_KERNEL);
    if (!info) {
        ret = -ENOMEM;
        goto fail_info;
    }

    info->battery_desc.name = kasprintf(GFP_KERNEL, "%s-%d", client->name, num);
    if (!info->battery_desc.name) {
        ret = -ENOMEM;
        goto fail_name;
    }

    i2c_set_clientdata(client, info);
    info->client = client;
    info->id = num;
    abigail_power_supply_init(&info->battery_desc);
    psy_cfg.drv_data = info;
    info->capacity = 100;
    info->status = POWER_SUPPLY_STATUS_FULL;

    INIT_DELAYED_WORK(&info->bat_work_queue, abigail_bat_work);
    info->battery = power_supply_register(&client->dev, 
                            &info->battery_desc, &psy_cfg);
    
    if (IS_ERR(info->battery)) {
        dev_err(&client->dev, "failed to register battery\n");
        ret = PTR_ERR(info->battery);
        goto fail_register;
    } else {
        schedule_delayed_work(&info->bat_work_queue, POLL_DELAY);
    }

    info->usb_desc.properties = abigail_usb_props;
    info->usb_desc.num_properties = ARRAY_SIZE(abigail_usb_props);
    info->usb_desc.get_property = abigail_usb_get_property;
    info->usb_desc.name = "usb";
    info->usb_desc.type = POWER_SUPPLY_TYPE_MAINS;

    info->usb = power_supply_register(NULL, &info->usb_desc, &psy_cfg);
    if (IS_ERR(info->usb)) {
        ret = PTR_ERR(info->usb);
        return ret;
    }

    return 0;

fail_register:
    kfree(info->battery_desc.name);
fail_name:
    kfree(info);
fail_info:
    mutex_lock(&battery_lock);
	idr_remove(&battery_id, num);
	mutex_unlock(&battery_lock);
fail_id:
    return ret;
}

static int abigail_battery_remove(struct i2c_client *client)
{
    struct abigail_info *info = i2c_get_clientdata(client);

    power_supply_unregister(info->battery);
    kfree(info->battery_desc.name);

    mutex_lock(&battery_lock);
	idr_remove(&battery_id, info->id);
	mutex_unlock(&battery_lock);

	cancel_delayed_work(&info->bat_work_queue);

	kfree(info);

    return 0;
}

#ifdef CONFIG_OF

static const struct of_device_id abigail_of_match[] = {
    {
        .compatible = "videray,abigail-battery",
        .data = (void*) 0,
    },
    {}
};

MODULE_DEVICE_TABLE(of, abigail_of_match);

#endif // CONFIG_OF


#ifdef CONFIG_PM_SLEEP

static int abigail_suspend(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct abigail_info *info = i2c_get_clientdata(client);

	cancel_delayed_work(&info->bat_work_queue);
	return 0;
}

static int abigail_resume(struct device *dev)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct abigail_info *info = i2c_get_clientdata(client);

	schedule_delayed_work(&info->bat_work_queue, POLL_DELAY);
	return 0;
}

static SIMPLE_DEV_PM_OPS(abigail_battery_pm_ops, abigail_suspend, abigail_resume);

#endif /* CONFIG_PM_SLEEP */



static const struct i2c_device_id abigail_id[] = {
    { "abigail-battery", 0 },
    {}
};
MODULE_DEVICE_TABLE(i2c, abigail_id);

static struct i2c_driver abigail_battery_driver = {
    .driver = {
        .name = "abigail-battery",
#ifdef CONFIG_PM_SLEEP
        .pm = &abigail_battery_pm_ops,
#endif // CONFIG_PM_SLEEP
#ifdef CONFIG_OF
        .of_match_table = of_match_ptr(abigail_of_match),
#endif // CONFIG_OF
    },
    .probe = abigail_battery_probe,
    .remove = abigail_battery_remove,
    .id_table = abigail_id,
};
module_i2c_driver(abigail_battery_driver);

MODULE_AUTHOR("Paul Soucy paul.soucy@videray.com");
MODULE_DESCRIPTION("Battery Driver for Videray PX1");
MODULE_LICENSE("GPL");