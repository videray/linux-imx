/*
 * i2c_commands.h
 *
 *  Created on: Jan 10, 2019
 *      Author: AliRezaei
 */

#ifndef I2C_COMMANDS_H_
#define I2C_COMMANDS_H_

#define abigail_i2c_address 0x60
#define PMT_R_VIPER_ADDRESS	0b0101111<<1
#define PMT_L_VIPER_ADDRESS	0b0101110<<1
#define PMT_L_ADC_ADDRESS	0b0010100<<1
#define PMT_R_ADC_ADDRESS	0b1010100<<1
#define TUBE_ADC_ADDRESS	0b0010000<<1
#define CHARGER_ADDRESS		0b0001001<<1
#define BATTERY_ADDRESS		0b0001011<<1
#define RGB_GC_ADDRESS		0b1000000<<1
#define RGB_R_ADDRESS		0b0110101<<1
#define RGB_L_ADDRESS		0b0110010<<1
#define FAN_EX_ADDRESS		0b1100010<<1
#define FAN_CPU_ADDRESS		0b1100001<<1
#define FAN_AUX_ADDRESS		0b1100000<<1
#define FLASH_ADDRESS		0b1010011<<1

typedef enum{
	NOT_A_COMMAND = 0U,
	SET_TUBE_VOLTAGE,					//followed by the tube voltage setting
	SET_TUBE_CURRENT,					//followed by the tube current settings
	SET_PMT_R_VOLTAGE,					//followed by the right PMT voltage setting
	SET_PMT_L_VOLTAGE,					//followed by the left PMT voltage setting
	SET_MOTOR_RPM,						//		/\	- start of FPGA commands
	SET_SPOKE_LOC_0,					//		||	-
	SET_SPOKE_LOC_1,					//		||	-
	SET_SPOKE_LOC_2,					//		||	-
	SET_SPOKE_LOC_3,					//		||	-
	SET_SPOKE_LOC_4,					//		||	-
	SET_SPOKE_LOC_5,					//		||	-
	SET_SPOKE_LOC_6,					//		||	-
	SET_SPOKE_LOC_7,					//		||	-
	SET_PIXEL_PER_LINE,					//		\/	- end of FPGA commands
	SET_CPU_TEMP,						//Followed by SOM temperature in degrees C
	SET_INSTRUMENT_CMND,				//followed by instrument commands
	//request commands start from 32769 (2^15+1)
	GET_STSTEM_STATUS_1  = 32769U,		//general system status, each bit refers to a specific peripheral
	GET_STSTEM_STATUS_2,
	GET_STSTEM_STATUS_3,
	GET_STSTEM_STATUS_4,
	GET_TUBE_STATUS,
	GET_TUBE_VOLTAGE_REF,
	GET_TUBE_CURRENT_REF,
	GET_TUBE_ACTUAL_VOLTAGE,
	GET_TUBE_ACTUAL_CURRENT,
	GET_TUBE_TEMPERATURE,
	GET_PMT_STATUS,
	GET_PMT_R_STATUS,
	GET_PMT_L_STATUS,
	GET_PMT_R_VOLTAGE_REF,
	GET_PMT_L_VOLTAGE_REF,
	GET_PMT_R_VOLTAGE_ACT,
	GET_PMT_L_VOLTAGE_ACT,
	GET_MOTOR_STATUS,
	GET_MOTOR_RPM_REF,
	GET_MOTOR_ACTUAL_RPM,
	GET_ABIGAIL_TEMPERATURE,
	GET_1V8_ACTUAL_VOLTAGE,
	GET_3V3_ACTUAL_VOLTAGE,
	GET_3V8_ACTUAL_VOLTAGE,
	GET_5V0_ACTUAL_VOLTAGE,
	GET_12V0_ACTUAL_VOLTAGE,
	GET_28V0_ACTUALVOLTAGE,
	GET_VBUS_ACTUAL_VOLTAGE,
	GET_CHARGER_STATUS,
	GET_BATT_SOC,
	GET_TRIGGER_STATUS,
	GET_ABIGAIL_VERSION = 61680U,
	GET_FPGA_VERSION = 65280U,
}Abigail_I2C_Command;

typedef enum{
	//do not add from start to leds off
	NOT_A_VALUE = 0U,
	INSTRUMENT_CMND_XRAY_HEARTBEAT,				//		/\	- start of led commands
	INSTRUMENT_CMND_LEDS_STARTUP,				//		||	-
	INSTRUMENT_CMND_LEDS_XRAY_ON,				//		||	-
	INSTRUMENT_CMND_LEDS_PATTERN_2,				//		||	-
	INSTRUMENT_CMND_LEDS_PATTERN_3,				//		||	-
	INSTRUMENT_CMND_LEDS_XRAY_ARMED,			//		||	-
	INSTRUMENT_CMND_LEDS_XRAY_SAFE,				//		||	-
	INSTRUMENT_CMND_LEDS_PATTERN_6,				//		||	-
	INSTRUMENT_CMND_LEDS_PATTERN_7,				//		||	-
	INSTRUMENT_CMND_LEDS_GREEN, 				//		||	-
	INSTRUMENT_CMND_LEDS_RED, 					//		||	-
	INSTRUMENT_CMND_LEDS_BLINK_RED_SLOW,		//		||	-
	INSTRUMENT_CMND_LEDS_BLINK_RED_FAST,		//		||	-
	INSTRUMENT_CMND_LEDS_BLINK_GREEN_SLOW,		//		||	-
	INSTRUMENT_CMND_LEDS_BLINK_GREEN_FAST,		//		||	-
	INSTRUMENT_CMND_LEDS_OFF,					//		\/	- end of led commands
	INSTRUMENT_CMND_ARM,
	INSTRUMENT_CMND_DEARM,
	INSTRUMENT_CMND_CLEAR_SETTINGS,
	INSTRUMENT_CMND_START_MOTOR,				//		/\	- start of FPGA commands
	INSTRUMENT_CMND_STOP_MOTOR,					//		||	-
	INSTRUMENT_CMND_ADC_ON,						//		||	-
	INSTRUMENT_CMND_ADC_OFF,					//		||	-
	INSTRUMENT_CMND_FPGA_ON,					//		||	-
	INSTRUMENT_CMND_FPGA_OFF,					//		\/	- end of FPGA commands
	INSTRUMENT_CMND_TUBE_ARM,					//		/\	- start of tube commands
	INSTRUMENT_CMND_TUBE_ON,					//		||	-
	INSTRUMENT_CMND_TUBE_OFF,					//		||	-
	INSTRUMENT_CMND_TUBE_LONG_WARMUP,			//		||	-
	INSTRUMENT_CMND_TUBE_DAILY_WARMUP,			//		\/	- end of tube commands
	INSTRUMENT_CMND_PMT_ON,						//		/\	- start of PMT commands
	INSTRUMENT_CMND_PMT_OFF,					//		\/	- end of PMT commands
	INSTRUMENT_CMND_LASER_ON,					//		/\	- start of instrument commands
	INSTRUMENT_CMND_LASER_OFF,					//		||	-
	INSTRUMENT_CMND_FLASH_ON,					//		||	-
	INSTRUMENT_CMND_FLASH_OFF,					//		||	-
	INSTRUMENT_CMND_TRIG_ENABLE,				//		||	-
	INSTRUMENT_CMND_TRIG_DISABLE,				//		||	-
	INSTRUMENT_CMND_TURN_OFF,					//		||	-
	INSTRUMENT_CMND_ABIGAIL_UPDATE = 21845,		//		||	-
	INSTRUMENT_CMND_FPGA_UPDATE = 43690,		//		\/	- end of instrument commands
}Abigail_Set_Instrument_command;

typedef enum{
	NOT_A_SOURCE = 0U,
	FROM_TUBEHH,
	FROM_SOM
}Message_Source;

#endif /* I2C_COMMANDS_H_ */
